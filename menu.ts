import { Color, Menu as MenuSDK } from "wrapper/Imports"
import { VISIBLE_DATA } from "./data"

const CreateMenu = (name_particle: string[], name: string = "VisibleByEnemy") => {
	const Menu = MenuSDK.AddEntryDeep(["Visual", name])
	const State = Menu.AddToggle("State", true, "State script: ON or OFF")
	const IColor = Menu.AddColorPicker("Color", Color.Aqua)
	const Switcher = Menu.AddDropdown("Select Effect", name_particle, name !== "VisibleByEnemy" ? 7 : 0)
	if (Switcher.selected_id !== 0)
		IColor.IsHidden = true
	const ShowOnAll = Menu.AddToggle("On units", false, "Show on: wards, creeps, heroes, and all buildings")
	const ShowOnSelf = Menu.AddToggle("On self", true, "Show on self")
	const ShowOnWards = Menu.AddToggle("On wards", true, "Show on wards")
	const ShowOnMines = Menu.AddToggle("On mines", true, "Show on mines")
	const ShowOnCreeps = Menu.AddToggle("On creeps", true, "Show on creeps")
	const ShowOnAllies = Menu.AddToggle("On heroes", true, "Show on heroes")
	const ShowOnTowers = Menu.AddToggle("On towers", true, "Show on towers")
	const ShowOnBuilding = Menu.AddToggle("On all buildings", false, "Show on all buildings")
	return {
		Menu,
		State,
		IColor,
		Switcher,
		ShowOnAll,
		ShowOnSelf,
		ShowOnMines,
		ShowOnWards,
		ShowOnCreeps,
		ShowOnTowers,
		ShowOnAllies,
		ShowOnBuilding,
	}
}

export const VBE = CreateMenu(VISIBLE_DATA.VBEMenuNameEffect)
export const TrueSight = CreateMenu(VISIBLE_DATA.TSMenuNameEffect, "TrueSight")

VBE.Switcher.OnValue(({ selected_id }) => {
	VBE.IColor.IsHidden = selected_id !== 0
})

TrueSight.Switcher.OnValue(({ selected_id }) => {
	TrueSight.IColor.IsHidden = selected_id !== 0
})

MenuSDK.Localization.AddLocalizationUnit("russian", new Map([
	["On units", "На юнитах"],
	["On self", "На себе"],
	["On wards", "На вардах"],
	["On creeps", "На крипах"],
	["On towers", "На таверах"],
	["On heroes", "На героях"],
	["On mines", "На минах"],
	["TrueSight", "Индикатор True Sight"],
	["VisibleByEnemy", "Индикатор видимости"],
	["Select Effect", "Выберите эффект"],
	["On all buildings", "На всех строениях"],
	["Show on self", "Показывать на себе"],
	["Show on mines", "Показывать на минах"],
	["Show on wards", "Показывать на вардах"],
	["Show on towers", "Показывать на таверах"],
	["Show on creeps", "Показывать на крипах"],
	["Show on heroes", "Показывать на героях"],
	["Show on all buildings", "Показывать на всех строениях"],
	["Show on: wards, creeps, heroes, and all building", "Рисует на вардах, героях, крипах и на всех строениях"],
]))
