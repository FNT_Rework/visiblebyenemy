import { EntityManager, Menu, ParticlesSDK, Unit } from "wrapper/Imports"

export interface MenuType {
	Menu: Menu.Node
	State: Menu.Toggle
	IColor: Menu.ColorPicker
	Switcher: Menu.Dropdown
	ShowOnAll: Menu.Toggle
	ShowOnSelf: Menu.Toggle
	ShowOnWards: Menu.Toggle
	ShowOnMines: Menu.Toggle
	ShowOnCreeps: Menu.Toggle
	ShowOnTowers: Menu.Toggle
	ShowOnAllies: Menu.Toggle
	ShowOnBuilding: Menu.Toggle
}

export class VISIBLE_DATA {

	public static VParticlesSDK = new ParticlesSDK()
	public static TParticlesSDK = new ParticlesSDK()

	public static ExcludeClassName: string[] = [
		"",
		"CDOTA_Unit_Fountain",
		"CDOTA_Unit_Announcer",
		"CDOTA_Unit_Earth_Spirit_Stone",
		"CDOTA_BaseNPC_NeutralItemStash",
		"CDOTA_Unit_Announcer_Killing_Spree",
	]

	public static readonly VBEparticles = [
		"particles/vbe/aura_shivas.vpcf",
		"particles/ui/ui_sweeping_ring.vpcf",
		"particles/units/heroes/hero_omniknight/omniknight_heavenly_grace_beam.vpcf",
		"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner_status.vpcf",
		"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner_dark.vpcf",
		"particles/units/heroes/hero_oracle/oracle_fortune_purge.vpcf",
		"particles/units/heroes/hero_spirit_breaker/spirit_breaker_haste_owner_timer.vpcf",
	]

	public static readonly VBEMenuNameEffect = ["Shiva", "Radial", "Beam", "Beam light", "Dark", "Purge", "Timer"]

	public static get TrueSightParticles() {
		return [...this.VBEparticles, "gitlab.com/FNT_Rework/x-core/scripts_files/particles/true_sight/ward_true_sight_true_sight.vpcf"]
	}

	public static get TSMenuNameEffect() {
		return [...this.VBEMenuNameEffect, "Eye Wards"]
	}

	public static VMenuDispose() {
		EntityManager.GetEntitiesByClass(Unit)
			.forEach(unit => this.VParticlesSDK.DestroyByKey(unit))

	}

	public static TMenuDispose() {
		EntityManager.GetEntitiesByClass(Unit)
			.forEach(unit => this.TParticlesSDK.DestroyByKey(unit))
	}

	public static Dispose() {
		EntityManager.GetEntitiesByClass(Unit).forEach(unit => {
			this.VParticlesSDK.DestroyByKey(unit)
			this.TParticlesSDK.DestroyByKey(unit)
		})
	}
}
