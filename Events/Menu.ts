import { VISIBLE_DATA } from "../data"
import { TrueSight, VBE } from "../menu"

VBE.State.OnDeactivate(() =>
	VISIBLE_DATA.VMenuDispose())

TrueSight.State.OnDeactivate(() =>
	VISIBLE_DATA.TMenuDispose())
