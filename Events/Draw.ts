import { GameX } from "immortal-core/Imports"
import { Color, Creep, EntityManager, EventsSDK, Hero, ParticleAttachment_t, TechiesMines, Unit } from "wrapper/Imports"
import { VISIBLE_DATA } from "../data"
import { TrueSight } from "../menu"
import { VValidation } from "./validate"

const color = new Color(145, 212, 255)
EventsSDK.on("Draw", () => {
	if (!GameX.IsInGame || !TrueSight.State.value || TrueSight.Switcher.selected_id !== 7)
		return
	EntityManager.GetEntitiesByClass(Unit).forEach(unit => {
		if (VISIBLE_DATA.ExcludeClassName.includes(unit.ClassName)
			|| !(unit instanceof Hero || unit instanceof Creep || unit instanceof TechiesMines)
			|| unit.IsEnemy())
			return
		if (!unit.IsAlive || !VValidation.IsUnitShouldBeHighlighted(unit, TrueSight) || !unit.IsTrueSightedForEnemies) {
			VISIBLE_DATA.TParticlesSDK.DestroyByKey(unit)
			return
		}
		const TypePathSDK = VISIBLE_DATA.TrueSightParticles[TrueSight.Switcher.selected_id]
		const position = unit.Position.Clone().AddScalarZ(Camera.Distance / 9)

		if (VISIBLE_DATA.TParticlesSDK.AllParticles.has(unit)) {
			VISIBLE_DATA.TParticlesSDK.SetConstrolPointsByKey(unit, [0, position], [1, color])
			return
		}
		VISIBLE_DATA.TParticlesSDK.AddOrUpdate(unit, TypePathSDK, ParticleAttachment_t.PATTACH_ABSORIGIN, unit,
			[0, position],
			[1, color],
		)
	})
})
