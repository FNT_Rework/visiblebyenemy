import { Building, Creep, Hero, LocalPlayer, Outpost, TechiesMines, Tower, Unit, WardObserver, WardTrueSight } from "wrapper/Imports"
import { MenuType } from "../data"

export class VValidation {

	public static IsUnitShouldBeHighlighted = (unit: Unit, menu: MenuType) => {
		if (unit instanceof Hero) {
			if (menu.ShowOnSelf.value && unit.Owner === LocalPlayer)
				return true
			if (menu.ShowOnAllies.value && unit.Owner !== LocalPlayer)
				return true
		}

		if (unit instanceof TechiesMines && menu.ShowOnMines.value)
			return true

		if (unit instanceof Creep && menu.ShowOnCreeps.value)
			return true

		if ((unit instanceof WardTrueSight || unit instanceof WardObserver) && menu.ShowOnWards.value)
			return true

		if ((unit instanceof Building) && menu.ShowOnBuilding.value)
			return true

		if ((unit instanceof Tower || unit instanceof Outpost) && menu.ShowOnTowers.value)
			return true

		return menu.ShowOnAll.value
	}
}
