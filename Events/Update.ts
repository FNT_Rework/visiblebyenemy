import { GameX } from "immortal-core/Imports"
import { Building, Color, Entity, EntityManager, EventsSDK, LifeState_t, ParticleAttachment_t, Shop, Unit, WardObserver, WardTrueSight } from "wrapper/Imports"
import { MenuType, VISIBLE_DATA } from "../data"
import { TrueSight, VBE } from "../menu"
import { VValidation } from "./validate"

const color = new Color(145, 212, 255)
const VisibleByType = (unit: Unit, menuType: MenuType, VisibleType: boolean) => {
	if (!GameX.IsInGame || (!VBE.State.value && !TrueSight.State.value))
		return

	if (!menuType.State.value || unit === undefined || !unit.IsValid)
		return

	const TypeSDK = menuType.Menu.InternalName === "VisibleByEnemy"
		? VISIBLE_DATA.VParticlesSDK
		: VISIBLE_DATA.TParticlesSDK

	const TypePathSDK = menuType.Menu.InternalName === "VisibleByEnemy"
		? VISIBLE_DATA.VBEparticles[menuType.Switcher.selected_id]
		: VISIBLE_DATA.TrueSightParticles[menuType.Switcher.selected_id]

	if (!unit.IsAlive || !VValidation.IsUnitShouldBeHighlighted(unit, menuType) || !VisibleType) {
		TypeSDK.DestroyByKey(unit)
		return
	}

	if (TypePathSDK === VISIBLE_DATA.TrueSightParticles[7]) {
		if (unit instanceof WardObserver || unit instanceof WardTrueSight || unit instanceof Building) {
			const position = unit.Position.Clone().AddScalarZ(Camera.Distance / 20)
			if (unit instanceof WardTrueSight)
				position.AddScalarZ(75)
			if (unit instanceof Building)
				position.AddScalarZ(100)
			TypeSDK.AddOrUpdate(
				unit,
				TypePathSDK,
				ParticleAttachment_t.PATTACH_ABSORIGIN,
				unit,
				[0, position],
				[1, color],
			)
		}
		return
	}

	TypeSDK.AddOrUpdate(unit, TypePathSDK, ParticleAttachment_t.PATTACH_ABSORIGIN_FOLLOW, unit,
		[1, menuType.IColor.selected_color],
		[2, menuType.IColor.selected_color.a],
	)
}

function UpdateUnit(unit: Entity | Unit) {
	if (unit instanceof Unit && !(unit instanceof Shop) && !VISIBLE_DATA.ExcludeClassName.includes(unit.ClassName) && (!unit.IsEnemy() || unit.IsNeutral)) {
		VisibleByType(unit, VBE, unit.IsVisibleForEnemies)
		VisibleByType(unit, TrueSight, unit.IsTrueSightedForEnemies)
	}
}

const MenuUpdate = () => 
	EntityManager.GetEntitiesByClass(Unit).forEach(UpdateUnit)

VBE.State.OnValue(MenuUpdate)
VBE.IColor.OnValue(MenuUpdate)
VBE.ShowOnAll.OnValue(MenuUpdate)
VBE.ShowOnAllies.OnValue(MenuUpdate)
VBE.ShowOnBuilding.OnValue(MenuUpdate)
VBE.ShowOnCreeps.OnValue(MenuUpdate)
VBE.ShowOnMines.OnValue(MenuUpdate)
VBE.ShowOnSelf.OnValue(MenuUpdate)
VBE.ShowOnTowers.OnValue(MenuUpdate)
VBE.ShowOnWards.OnValue(MenuUpdate)
VBE.Switcher.OnValue(MenuUpdate)

TrueSight.State.OnValue(MenuUpdate)
TrueSight.ShowOnAll.OnValue(MenuUpdate)
TrueSight.ShowOnAllies.OnValue(MenuUpdate)
TrueSight.ShowOnBuilding.OnValue(MenuUpdate)
TrueSight.ShowOnCreeps.OnValue(MenuUpdate)
TrueSight.ShowOnMines.OnValue(MenuUpdate)
TrueSight.ShowOnSelf.OnValue(MenuUpdate)
TrueSight.ShowOnTowers.OnValue(MenuUpdate)
TrueSight.ShowOnWards.OnValue(MenuUpdate)
TrueSight.Switcher.OnValue(MenuUpdate)

EventsSDK.on("LifeStateChanged", unit => {
	if (unit.LifeState === LifeState_t.LIFE_DEAD)
		UpdateUnit(unit)
})

EventsSDK.on("LocalTeamChanged", () => MenuUpdate())
EventsSDK.on("EntityDestroyed", unit => UpdateUnit(unit))
EventsSDK.on("EntityTeamChanged", unit => UpdateUnit(unit))
EventsSDK.on("TrueSightedChanged", unit => UpdateUnit(unit))
EventsSDK.on("TeamVisibilityChanged", unit => UpdateUnit(unit))
